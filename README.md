# Font reflector archive

All files in this repository are available from the Internet Archive.

## Mirror of mscorefonts2 project

These files are mirrored from [mscorefonts2](http://mscorefonts2.sourceforge.net).

### Core fonts for the web

The Microsoft EULA for these fonts:
https://web.archive.org/web/20100109092750/http://www.microsoft.com/typography/fontpack/eula.htm

Original links are available from the [Wine Tricks project](https://github.com/Winetricks/winetricks/issues/1054).

Files:

* `andale32.exe`
* `arial32.exe`
* `arialb32.exe`
* `comic32.exe`
* `courie32.exe`
* `georgi32.exe`
* `impact32.exe`
* `times32.exe`
* `trebuc32.exe`
* `verdan32.exe`
* `webdin32.exe`

### Other files

Original links are available from the [Wine Tricks project](https://github.com/Winetricks/winetricks/issues/1054).

* `EUupdate.EXE` (from [Internet Archive](http://web.archive.org/web/20180413042538/http://download.microsoft.com/download/a/1/8/a180e21e-9c2b-4b54-9c32-bf7fd7429970/EUupdate.EXE))

* `IELPKTH.CAB` (from [Internet Archive](http://web.archive.org/web/20030208102938/http://download.microsoft.com:80/download/ie55sp2/Install/5.5_SP2/WIN98Me/EN-US/IELPKTH.CAB))


## Mirror of corefonts project

These files are mirrored from the [corefonts](https://sourceforge.net/projects/corefonts/files/the%20fonts/final/) project.

* Word 97 Viewer `wd97vwr32.exe` (from [SourceForge](https://sourceforge.net/projects/corefonts/files/the%20fonts/final/wd97vwr32.exe/download))


## Microsoft Euro Currency Support for Windows NT Server, Terminal Server Edition

Originally from: [Microsoft](http://www.microsoft.com/ntserver/terminalserver/downloads/other/TSEEuro/default.asp), [Internet Archive](https://web.archive.org/web/19990423095011/http://www.microsoft.com/ntserver/terminalserver/downloads/other/TSEEuro/default.asp)

* `eurofixi.exe` ([Internet Archive](http://web.archive.org/web/20030118044911/http://download.microsoft.com:80/msdownload/euro/wts/x86/eurofixi.exe))


## ClearType fonts for Windows XP

### Simplified Chinese ClearType fonts for Windows XP

Originally from: [Microsoft](https://www.microsoft.com/en-us/download/details.aspx?id=14577), [Internet Archive](https://web.archive.org/web/20120525091759/http://www.microsoft.com/en-us/download/details.aspx?id=14577)

* `VistaFont_CHS.EXE` ([Microsoft](https://download.microsoft.com/download/d/6/e/d6e2ff26-5821-4f35-a18b-78c963b1535d/VistaFont_CHS.EXE), [Internet Archive](https://web.archive.org/web/20101224175608if_/http://download.microsoft.com/download/d/6/e/d6e2ff26-5821-4f35-a18b-78c963b1535d/VistaFont_CHS.EXE))

### Traditional Chinese ClearType fonts for Windows XP

Originally from: [Microsoft](https://www.microsoft.com/en-us/download/details.aspx?id=12072), [Internet Archive](https://web.archive.org/web/20120507022909/http://www.microsoft.com/en-us/download/details.aspx?id=12072)

* `VistaFont_CHT.EXE` ([Microsoft](https://download.microsoft.com/download/7/6/b/76bd7a77-be02-47f3-8472-fa1de7eda62f/VistaFont_CHT.EXE), [Internet Archive](https://web.archive.org/web/20110609003721/http://download.microsoft.com/download/7/6/b/76bd7a77-be02-47f3-8472-fa1de7eda62f/VistaFont_CHT.EXE))

### Korean ClearType fonts for Windows XP

Originally from: [Microsoft KR](https://www.microsoft.com/ko-kr/download/details.aspx?id=10490), [Internet Archive](https://web.archive.org/web/20120508093816/https://www.microsoft.com/en-us/download/details.aspx?id=10490), [Microsoft (link down)](https://www.microsoft.com/en-us/download/details.aspx?id=10490)

* `VistaFont_KOR.EXE` ([Microsoft](http://download.microsoft.com/download/9/4/c/94c75092-7a45-46e2-9404-e9da9d068233/VistaFont_KOR.EXE), [Internet Archive](https://web.archive.org/web/20110609003907/http://download.microsoft.com/download/9/4/c/94c75092-7a45-46e2-9404-e9da9d068233/VistaFont_KOR.EXE))


### Japanese ClearType fonts for Windows XP

Originally from: [Microsoft (link down)](http://www.microsoft.com/en-us/download/details.aspx?id=10550), [Internet Archive](https://web.archive.org/web/20120508090531/http://www.microsoft.com/en-us/download/details.aspx?id=10550)

* `VistaFont_JPN.EXE` ([Microsoft](http://download.microsoft.com/download/7/0/1/70182c8c-e50d-4e31-8fd5-f5273a1a580e/VistaFont_JPN.EXE), [Internet Archive](https://web.archive.org/web/20130327105034/http://download.microsoft.com/download/7/0/1/70182c8c-e50d-4e31-8fd5-f5273a1a580e/VistaFont_JPN.EXE))


### Windows XP Font Pack for ISO 10646:2003 + Amendment 1 Traditional Chinese Support

Originally from: [Microsoft (link down)](http://www.microsoft.com/en-us/download/details.aspx?id=10109), [Internet Archive](https://web.archive.org/web/20120616055607/http://www.microsoft.com/en-us/download/details.aspx?id=10109)

* `HKSCS2004-x86.EXE` ([Microsoft](http://download.microsoft.com/download/6/1/4/6149F613-738D-4292-A6EB-65B9F50AFCB6/HKSCS2004-x86.EXE), [Internet Archive](https://web.archive.org/web/20120616055607/http://download.microsoft.com/download/6/1/4/6149F613-738D-4292-A6EB-65B9F50AFCB6/HKSCS2004-x86.EXE))


## GB18030 Support Package

Originally from: [Microsoft (link down)](https://www.microsoft.com/china/windows2000/downloads/18030.mspx), [Internet Archive](https://web.archive.org/web/20070910020705/https://www.microsoft.com/china/windows2000/downloads/18030.mspx)

* `GBEXTSUP.msi` (from [Microsoft](http://download.microsoft.com/download/whistler/Patch/10/NT5XP/CN/GBEXTSUP.msi), [Internet Archive](https://web.archive.org/web/20020305235115/http://download.microsoft.com/download/whistler/Patch/10/NT5XP/CN/GBEXTSUP.msi))


## Excel Viewer 2007

Originally from: [Microsoft (link down)](https://www.microsoft.com/en-us/download/details.aspx?id=10), [Internet Archive](https://web.archive.org/web/20120426220255/https://www.microsoft.com/en-us/download/details.aspx?id=10)

* `ExcelViewer.exe` (from [Internet Archive](https://web.archive.org/web/20171225133033if_/http://download.microsoft.com/download/e/a/9/ea913c8b-51a7-41b7-8697-9f0d0a7274aa/ExcelViewer.exe))


## PowerPoint Viewer 2007

Originally from: [Microsoft (link down)](http://www.microsoft.com/en-us/download/confirmation.aspx?id=13), [Internet Archive](https://web.archive.org/web/20120430003939/http://www.microsoft.com/en-us/download/confirmation.aspx?id=13)

* `PowerPointViewer.exe` (from [Microsoft](http://download.microsoft.com/download/f/5/a/f5a3df76-d856-4a61-a6bd-722f52a5be26/PowerPointViewer.exe), [Archive (2006)](https://web.archive.org/web/20061208145834/http://download.microsoft.com/download/f/5/a/f5a3df76-d856-4a61-a6bd-722f52a5be26/PowerPointViewer.exe), [Archive (2012)](https://web.archive.org/web/20120430003939/http://download.microsoft.com/download/E/6/7/E675FFFC-2A6D-4AB0-B3EB-27C9F8C8F696/PowerPointViewer.exe))


## Dengxian Font Pack

Originally from: [Microsoft](https://www.microsoft.com/en-us/download/details.aspx?id=49113), [Internet Archive](https://web.archive.org/web/20200802184253/https://www.microsoft.com/en-us/download/details.aspx?id=49113)

* `eadengfontpack.exe` (from [Internet Archive](https://web.archive.org/web/20200705160250/https://download.microsoft.com/download/4/0/9/4094ABA6-1A50-4FFD-A8CC-846BD66573F0/eadengfontpack.exe))


## Yu Gothic Yu Mincho Font Pack

Originally from: [Microsoft](https://www.microsoft.com/en-us/download/details.aspx?id=49114), [Internet Archive](https://web.archive.org/web/20151029225818/http://www.microsoft.com/en-us/download/details.aspx?id=49114)

* `eayufontpack.exe` (from [Internet Archive](https://web.archive.org/web/20200705162349/https://download.microsoft.com/download/E/3/2/E324A90F-5173-49F9-A9B4-FE2AFF558748/eayufontpack.exe))
